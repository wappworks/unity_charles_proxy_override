# Unity Charles Proxy Override Library (Android)

This Android proejct library adds an Android security configuration to accept transmissions from
an unsecure HTTPS proxy server.


## Requirements

* Gradle 6+


## Building the library (AAR)

* Set up the Gradle wrapper
  * In the project root, run `gradle wrapper`; this will generate the `gradlew` executable
    necesssary for building the library
* Assemble the library using the Gradle wrapper
  * Run `gradlew assembleRelease`; this assembles the release library artifact (AAR) in the
    `charlesproxyoverride/build/outputs/aar` directory


## Installing the library into a Unity project

* Copy the library file (AAR) in the Unity `Assets/Plugin/Android` project directory

We're done!
